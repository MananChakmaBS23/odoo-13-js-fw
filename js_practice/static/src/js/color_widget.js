odoo.define('color_widget', function(require){
    "use strict";

    var AbstractField = require('web.AbstractField');
    var fieldRegistry = require('web.field_registry');

    var core = require('web.core');
    var qweb = core.qweb;

    console.log('file loaded');

    var colorWidget= AbstractField.extend({
        className: 'o_char_colorpicker',
        tagName: 'span',
        supportedFieldTypes: ['char'],

        init: function() {
            this._super.apply(this, arguments);
            this.colorCodes = ["#ff5393", "#a3e4d7", "#fff68f"];
            this.totalColors = this.colorCodes.length;
        },

        events: {
            'click .o_color_pick' : 'clickPiker',
        },

        _renderEdit: function(){
            console.log('readedit method called');
            this.$el.empty();
            for(var i=0; i < this.totalColors; i++){
                var className = "o_color_pick o_color_"+ i;

                if(this.value === i){
                    className += ' active';
                }
                var $item = $('<span>',{
                        'class': className,
                        'data-val': this.colorCodes[parseInt(i)],
                        });
                $item.css({backgroundColor: this.colorCodes[parseInt(i)]});


                this.$el.append(
                   $item
                );
            }
        },

        _renderReadonly: function(){
            console.log('renderreadonly method called');
            var className = 'o_color_pick active readonly o_color_'+this.value;
            this.$el.append($('<span>',{
                'class': className,
            }));
        },


        clickPiker: function(ev){
            if(this.$prevTarget != undefined){
                this.$prevTarget.css({width: '25px', height: '25px'});
                this.$prevTarget = undefined;
            }
            var $target = $(ev.currentTarget);
            var data = $target.data();
            var colorCode = data.val.toString();
            $target.css({width: '30px', height: '30px'});
            this.$prevTarget = $target;
            console.log(colorCode);
        },


    });

    fieldRegistry.add('color_widget', colorWidget);

    return {
        ColorWidget: colorWidget,
    };

});