# -*- coding: utf-8 -*-
{
    'name': "js_practice",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Brainstation 23 LTD",
    'website': "http://www.yourcompany.com",

    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'purchase'],

    # always loaded
    'data': [
        'views/assets.xml',
        'views/views.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
