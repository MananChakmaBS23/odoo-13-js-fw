# -*- coding: utf-8 -*-

from odoo import models, fields, api


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    _description = 'js_practice.js_practice'

    color = fields.Char(string="Color")
